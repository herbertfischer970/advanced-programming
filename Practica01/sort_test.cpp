#include <iostream>
#include <vector>
#include <array>
#include <algorithm>

using namespace std;

/*
sort the list with countsort
*/
void countsort(vector<int> &vec);

int main()
{
    vector<int> abc = {3, 0, 2, 2, 0};
    countsort(abc);

    for (auto item : abc)
        cout << item << endl;

    return 0;
}

void countsort(vector<int> &vec)
{
    array<int, 10> counts = {};

    // count occurences
    for (int item : vec)
        counts[item]++;

    // overwrite list in a sorted manner
    int pos = 0;
    int counts_size = counts.size();

    for (int i = 0; i < counts_size; i++)
    {
        for (int c = 0; c < counts[i]; c++)
        {
            vec[pos] = i;
            pos++;
        }
    };
}
